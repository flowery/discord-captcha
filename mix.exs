defmodule DiscordCaptcha.MixProject do
  use Mix.Project

  def project do
    [
      app: :discord_captcha,
      version: "0.1.0",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: { DiscordCaptcha, [] },
      applications: [:jason, :httpoison, :cowboy, :ranch],
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:cowboy, "~> 2.9"},
      {:jason, "~> 1.2"},
      {:httpoison, "~> 1.7"}
    ]
  end
end
