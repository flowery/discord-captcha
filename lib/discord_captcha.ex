defmodule DiscordCaptcha do
   @moduledoc """
   Application for `DiscordCaptcha`.
   """

   def start(_type, _args) do
      routing_table = build_routing_table()
      {:ok, _} = :cowboy.start_clear(
         :http,
         [{:port, 2468}],
         %{env: %{dispatch: routing_table}}
      )
   end

   defp build_routing_table() do
      :cowboy_router.compile([
         {:_, [
            {"/", :cowboy_static, {:priv_file, :discord_captcha, "index.html"}},
            {"/static/[...]", :cowboy_static, {:priv_dir, :discord_captcha, "static"}},

            {"/interact", DiscordCaptcha.InteractionHandler, []}
         ]}
      ])
   end
end
