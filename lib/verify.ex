defmodule DiscordCaptcha.Verify do
   @public_hex "aa0ef97fe4e901856595a223998ecac37e74b113530ec193d4a0177336dfda5a"
   @public_key :binary.decode_unsigned(Base.decode16!(@public_hex, case: :lower))

   def verify(timestamp, body, signature) do
      case Base.decode16(signature, case: :lower) do
         :error -> false
         {:ok, signature} -> verify_with_keys(timestamp, body, signature)
      end
   end

   defp verify_with_keys(timestamp, body, signature) do
      :crypto.verify(:eddsa, :none, timestamp <> body, signature, [@public_key, :ed25519])
   end
end