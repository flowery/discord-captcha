defmodule DiscordCaptcha.InteractionHandler do
   @request_timeout 5

   def init(%{
      method: "POST",
      headers: %{
         "x-signature-ed25519" => signature,
         "x-signature-timestamp" => timestamp
      }
   } = req, state) do
      with {integer, ""} <- Integer.parse(timestamp),
           {:past, true} <- {:past, :os.system_time(:second) - integer < @request_timeout},
           {:ok, data, req} <- :cowboy_req.read_body(req),
           {:verify, true} <- {:verify, DiscordCaptcha.Verify.verify(timestamp, data, signature)},
           {:ok, body} <- Jason.decode(data)
      do
         res = handle_interaction(body)

         res = :cowboy_req.reply(
            200,
            %{"content-type" => "application/json"},
            Jason.encode!(res),
            req
         )

         {:ok, res, state}
      else
         :error -> response(401, "Timestamp should be an integer.", req, state)
         {:past, false} -> response(401, "Timestamp is too far in the past.", req, state)
         {:verify, false} -> response(401, "Could not authenticate request.", req, state)
         {:error, %Jason.DecodeError{}} -> response(400, "Invalid JSON passed.", req, state)
      end
   end

   def init(%{
      method: "POST",
      headers: %{
         "x-signature-ed25519" => _
      }
   } = req, state) do
      response(400, "Missing ed25519 timestamp.", req, state)
   end

   def init(%{
      method: "POST"
   } = req, state) do
      response(400, "Missing ed25519 signature.", req, state)
   end

   def init(req, state) do
      response(405, "Interactions only use POST.", req, state)
   end

   defp response(code, text, request, state) do
      res = :cowboy_req.reply(
         code,
         %{"content-type" => "application/text"},
         text,
         request
      )

      {:ok, res, state}
   end

   defp handle_interaction(%{"type" => 1}) do
      %{type: 1}
   end

   # captcha
   defp handle_interaction(%{
      "type" => 2,
      "data" => %{
         "id" => "867884386149466162"
      }
   }) do
      %{
         type: 4,
         data: %{
            content: "Click all the imposters.",
            components: DiscordCaptcha.Captcha.generate_captcha() ++ [DiscordCaptcha.Captcha.confirmation_button()]
         }
      }
   end

   defp handle_interaction(%{
      "type" => 3,
      "data" => %{
         "component_type" => 2,
         "custom_id" => <<1, 1, idx>>
      },
      "message" => %{
         "components" => components
      }
   }) do
      # emoji at idx was pressed
      idx = idx - 1

      captcha = DiscordCaptcha.Captcha.parse_captcha(components)
       |> Enum.reverse
       |> DiscordCaptcha.Captcha.patch_captcha(idx)
       |> DiscordCaptcha.Captcha.export_captcha


      captcha = captcha ++ [DiscordCaptcha.Captcha.confirmation_button()]

      %{
         type: 7,
         data: %{
            components: captcha 
         }
      }
   end

   defp handle_interaction(%{
      "type" => 3,
      "data" => %{
         "component_type" => 2,
         "custom_id" => <<1, 2, "submit">>
      },
      "message" => %{
         "components" => components
      },
      "application_id" => application_id,
      "token" => token
   }) do
      # submitted
      works = 811751280342597642 not in DiscordCaptcha.Captcha.parse_captcha(components)
      spawn(fn ->
         # race conditions, probably... if something fails, blame here.
         content = if works do
            "You did it!"
         else
            "Nope."
         end

         HTTPoison.delete!("https://discord.com/api/v9/webhooks/#{application_id}/#{token}/messages/@original")
         HTTPoison.post!("https://discord.com/api/v9/webhooks/#{application_id}/#{token}", Jason.encode!(%{
            content: content
         }), %{"Content-Type": "application/json"})
      end)
      %{
         type: 7,
         data: %{
            content: "blah"
         }
      }
   end
end
