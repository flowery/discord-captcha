defmodule DiscordCaptcha.Captcha do
   @emojis %{
      811751280342597642 => 0.5,
      808289597128966154 => 0.125,
      585803810916794368 => 0.125,
      810941610707517460 => 0.0625,
      "👺" => 0.0625,
      765221477251285012 => 0.0625,
      643544657044832286 => 0.0625
   }

   @names %{
      811751280342597642 => "amogus",
      808289597128966154 => "meowhuggies",
      585803810916794368 => "animePensive",
      810941610707517460 => "ramenmoyai",
      765221477251285012 => "brainthink",
      643544657044832286 => "drunkmegumin"
   }

   defp make_partial_emoji(emoji) when is_integer(emoji) do
      %{
         name: @names[emoji],
         id: emoji,
         animated: false
      }
   end

   defp make_partial_emoji(emoji) when is_binary(emoji) do
      %{
         name: emoji,
         id: nil,
         animated: false
      }
   end

   defp choose_emoji() do
      choose_emoji_helper(Map.keys(@emojis), 0, :rand.uniform())
   end

   defp choose_emoji_helper(keys, index, number) do
      if number < @emojis[Enum.at(keys, index)] do
         Enum.at(keys, index)
      else
         choose_emoji_helper(keys, index + 1, number - @emojis[Enum.at(keys, index)])
      end
   end

   defp construct_button(idx, emoji) do
      %{
         type: 2,
         custom_id: <<1, 1, idx + 1>>,
         style: 2,
         emoji: make_partial_emoji(emoji)
      }
   end

   def generate_captcha() do
      for i <- 0..2 do
         buttons = for j <- 0..2 do
            emoji = choose_emoji()
            construct_button(i * 3 + j, emoji)
         end

         %{
            type: 1,
            components: buttons
         }
      end
   end

   def parse_captcha(captcha) do
      parse_captcha_helper(captcha, [])
   end

   defp parse_captcha_helper([%{"type" => 1, "components" => buttons} | rest], acc) do
      parse_captcha_helper(rest, parse_buttons(buttons, acc))
   end

   defp parse_captcha_helper([], acc), do: acc

   defp parse_buttons([%{"type" => 2, "style" => 2, "emoji" => emoji}|rest], acc) do
      parse_buttons(rest, [parse_emoji(emoji)|acc])
   end

   defp parse_buttons([%{"type" => 2, "style" => 1, "label" => "Submit."}|rest], acc) do
      parse_buttons(rest, acc)
   end

   defp parse_buttons([], acc), do: acc

   defp parse_emoji(%{"id" => emoji}), do: String.to_integer(emoji)
   defp parse_emoji(%{"name" => emoji}), do: emoji

   def patch_captcha(captcha, idx) do
      captcha
      |> List.replace_at(idx, choose_emoji())
   end

   def export_captcha(captcha) do
      for i <- 0..2 do
         buttons = for j <- 0..2 do
            construct_button(i * 3 + j, Enum.at(captcha, i * 3 + j))
         end

         %{
            type: 1,
            components: buttons
         }
      end
   end

   def confirmation_button() do
      %{
         type: 1,
         components: [
            %{
               type: 2,
               custom_id: <<1, 2, "submit">>,
               style: 1,
               label: "Submit."
            }
         ]
      }
   end
end